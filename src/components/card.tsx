import React from 'react';
import { Link } from 'react-router-dom';
import './card.scss';

type Props = {
  id: number;
  title: string;
  img: string;
}

function Card(props: Props) {

  const {id, title, img} = props;

  return (
    <li className="card-item-container">
      <Link to={`/recipe/${id}`}>
      <div className="card">
        <div>
            <img src={img} alt={`Description of ${title}`}/>
        </div>
        <div className="card-content">
            <div className="card-title">
            {title}
          </div>
        </div>
        </div>
      </Link>
    </li>
  );
}

export default Card;
