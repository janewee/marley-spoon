
import { appConfig } from '../config'
export const contentful = require('contentful');

const client = contentful.createClient({
    space: appConfig.spaceId,
    accessToken: appConfig.accessToken,
});

export function retrieveRecipeList (contentType: string) {
    return client.getEntries({
        content_type: contentType
    }).then(function (entries: any) {
        return entries.items
    });
}

export function retrieveRecipeDetail (id: any) {
    return client.getEntries({
        'sys.id': id
    }).then(function (entries: any) {
        return entries.items[0].fields
    });
}

