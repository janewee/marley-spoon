import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { retrieveRecipeDetail } from '../../service/recipe-service';
import './recipe-detail.scss';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';

type Fields = {
    fields: {
        name: string
    }
}

type Image = {
    fields: {
        file: {
            url: string
        }
    }
}

type Recipe = {
    title: string;
    description: string;
    photo: Image;
    tags: Array<Fields>;
    chef: Fields;
}

function RecipeDetail() {

    let {id} = useParams()

    const [recipeDetail, setRecipeDetail] = useState<Recipe>({
        title: '',
        description: '',
        photo: {
            fields: {
                file: {
                    url: ''
                }
            }
        },
        tags: [],
        chef: {
            fields: {
                name: ''
            }
        },
    })

    useEffect(() => {
        retrieveRecipeDetail(id).then((response: any) => {
            setRecipeDetail(response)
        })
    }, [id])

    return (
        <div className="main">
            <div className="back-url">
                <Link to={`/recipe`}>&#10094; {' '} Recipe List</Link>
            </div>
            <div className="recipe-detail-container">
                <div className="recipe-img-container">
                    <img src={recipeDetail.photo.fields.file.url} alt={`Description of ${recipeDetail.title}`}/>
                </div>
                <div className="recipe-details">
                    <div className="recipe-title">
                        {recipeDetail.title}
                    </div>
                    <div className="recipe-description">
                        <ReactMarkdown>{recipeDetail.description}</ReactMarkdown>
                    </div>
                    <div className="recipe-tags-container">
                        {recipeDetail.tags && recipeDetail.tags.map((item: any, index: number) => (
                            <div className="recipe-tags" key={index}>{item.fields.name}</div>
                        ))}
                    </div>
                    <div className="recipe-chef">
                        Prepared by{' '}{recipeDetail.chef ? recipeDetail.chef.fields.name : 'Our Team at Marley Spoon'}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default RecipeDetail;
