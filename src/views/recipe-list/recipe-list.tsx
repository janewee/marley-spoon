import React, { useEffect, useState } from 'react';
import Card from '../../components/card';
import { retrieveRecipeList } from '../../service/recipe-service';
import './recipe-list.scss';

function RecipeList() {

    const [recipeList, setRecipeList] = useState([])

    useEffect(() => {
        retrieveRecipeList('recipe').then((response: any) => {
            setRecipeList(response)
        })
    }, [])

    return (
        <div className="main">
            <ul className="recipe-list-container">
                {recipeList.map((item: any, index: number) => (
                    <Card key={item.sys.id} id={item.sys.id} title={item.fields.title} img={item.fields.photo.fields.file.url}/>
                ))}
            </ul>
        </div>
    );
}

export default RecipeList;
