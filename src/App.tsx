import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import RecipeDetail from './views/recipe-detail/recipe-detail';
import RecipeList from './views/recipe-list/recipe-list';

function App() {
  
  return (
    <Router>
        <Routes>
          <Route path="/" element={<Navigate to="/recipe"/>}></Route> 
          <Route path="/recipe" element={<RecipeList/>}></Route> 
          <Route path="/recipe/:id" element={<RecipeDetail/>}></Route> 
        </Routes>
    </Router>
  );
}

export default App;
