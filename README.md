# Marley Spoon Frontend Engineering Challenge

This project is an app to consume data from Contentful API and display the data fetched within the list view and detailed view. 

This app is built using React framework with TypeScript, the UI styling is written using SCSS. 

## Available Scripts

In the project directory, you can run:

### `npm install`

This is to install the node_modules dependencies needed to run the app.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Package Dependency

These are the following packages used within the application and their usage:

  - **react-router** (For route navigation of pages within react)
  - **node-sass** (To write the styling in SCSS format)
  - **react-markdown** (To render the markdown for description)
  - **contentful** (For fetching API from Contentful)
  - **typescript** (For writing React with TypeScript in tsx file)

## Deliverables

There are 2 views within this project and you can navigate between the list view and details view. When clicking on a recipe on the list view, you should then navigate to the details view. 

### List view
- Display all the data for a recipe:
  - Title
  - Image
  - List of Tags
  - Description (with Markdown rendered)
  - Chef Name

### Details View
- Display all the data for a recipe:
  - Title
  - Image
  - List of Tags
  - Description (with Markdown rendered)
  - Chef Name


## Credentials
The API data fetched for Marley Spoon recipes is from [Contentful](https://contentful.com) (Content Delivery API).

The **Space ID** is: `kk2bw5ojx476` <br/>
The **Access Token** is: `7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c`<br/>



